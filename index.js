import express from 'express';
import sqlite3 from 'sqlite3';
import {open} from 'sqlite';

(async () => {
    const app = express(), port = 5000;
    app.set("view engine", "ejs");

    const db = await open({filename: "./data/GB.db", driver: sqlite3.Database});
    const query = await db.prepare(
        `SELECT name, latitude, longitude FROM location 
            WHERE name LIKE @name OR asciiname LIKE @name 
            ORDER BY length(name) ASC`,
    );

    app.get("/", (req, res, next) => res.render("index", {result: {}}));

    app.get("/locations", async (req, res, next) => {
        if (req.query.q === undefined) return res.render("index", {result: {err: "param `q` missing"}});
        if (typeof req.query.q !== "string") return res.render("index", {result: {err: "search term must be a string"}});
        if (req.query.q.length < 2) return res.render("index", {result: {err: "min 2 chars required"}});

        try {
            const locations = await query.all({"@name": req.query.q + "%"});
            return res.render("index", {result: {locations: locations}});
        } catch (e) {
            return res.render("index", {result: {err: "internal server error"}});
        }
    });

    app.listen(port, () => console.log(`Serving on localhost:${port}.`));
})();
