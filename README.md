## Dependencies

To enable the use of ES6 imports, first install Node.js 13 or above.

```bash
    git clone https://gitlab.com/cjdm1994/node-code-challenge.git
    npm install nodemon -g
    npm install
```

## Create database

```bash
    sqlite3 GB.db
    sqlite> .mode tabs
    sqlite> .import GB.tsv locations
```
## Run

You can start the server with Nodemon, which will perform restarts whenever you save a file:

`nodemon`

## Notes

For a larger application, I would of course modularise:

- Use a router
- Model-service-controller, or some other design pattern, to separate concerns
- Use some kind of auth and/or validation middleware to be shared across endpoints

I would also add unit tests with mocked dependencies, add integration tests for API endpoints and database queries, decouple the API from the client, use TypeScript, add indexes to the database, add good documentation etc.  

For this small task, though, I've tried to keep the solution as simple as possible. 
