# Questions

### Overview

I didn't know all the answers straight off the bat. Digged into JS/played with Chrome's debugger.
***

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Qs1 Answer:

- Output: `2`, `1`.
- Why: `setTimeout` is executed first, and the callback function is queued. `console.log("2")` is executed second. Finally the callback executes 100ms later.

***

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

Qs2 Answer:

- Output: `10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0`
- Why: `foo` is called 11 times until the recursion terminates with `foo(10)`. Moving down the stack, `foo(10)` through to `foo(0)` can now be executed, including each `console.log` call.
  
***

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

Qs3 Answer:

If `d` is falsy, e.g. `undefined`, `null`, `false`, `""`, `0`, it will be reassigned the value 5.

We could prevent zero being reassigned as 5, with `console.log(d === undefined ? 5 : d)`.
***

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Qs4 Answer:

- Output: `3`.
- Why: The anonymous function has access to the variable `a` in the scope of `foo`, so `bar` returns `1+2`.

***

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Qs5 Answer:

You would pass an anonymous or named callback to `double`. This callback would do something with the doubled value following completion of the sync operation.
```js
    double(50, (b) => console.log(b)) // e.g. 1
    double(50, someNamedFunc) // e.g. 2
```